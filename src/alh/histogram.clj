(ns alh.histogram)

(defn histogram-for-log-files
  "Produce an ascii histogram from the m, for which:
  {filename(string) -> count-of-requests-with-http-code(int)}.
  An empty histogram will return string 'No files in directory.'"
  [m]
  {:pre [(map? m)]
   :post [(string? %)]}
  "No files in directory.")
