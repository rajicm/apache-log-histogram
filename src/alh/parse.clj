(ns alh.parse

  (:require [me.raynes.fs :as fs])
  (:require [clj-time.core :as ct]))
(def clf-regex #"^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) [\w-]+ ([\w-]+) \[([\w\d/:\-+ ]+)\] \"(\w+) (/[^\"]*) HTTP/1.\d\" (\d{3}) (\d+)")

(defn parse-log-line
  [log-line]
  (let [[_ ip _ request-date method path code size] (re-find clf-regex log-line)]
    {:ip ip :request-date request-date :method method :path path :code code}))

(defn log-directory->http-code-count
  "Parse all files within the given directory, and returns a map:
  {filename(string) -> count-of-requests-with-http-code(int)}"
  [http-code log-file-directory]
  {:pre [(string? http-code) (string? log-file-directory)]
   :post [(map? %) (every? string? (keys %)) (every? integer? (vals %))]}
  {})
