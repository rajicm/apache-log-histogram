(ns alh.parse-test
  (:require [clojure.test.check :as tc]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]
            [clojure.test.check.clojure-test :refer [defspec]]
            [clj-time.core :as ct]
            [clj-time.format :as ctf]
            [clj-time.coerce :as ctc]
            [me.raynes.fs :as fs]
            [alh.parse :refer :all]))

(def eight-bit-number-gen (gen/fmap int gen/char))
(def ip-gen (gen/fmap #(clojure.string/join "." %)
                      (apply gen/tuple (repeat 4 eight-bit-number-gen))))
(def date-long-gen (gen/large-integer* {:min 1420070400000}))
(def method-gen (gen/elements #{"GET" "POST" "PUT" "HEAD" "OPTION" "DELETE"}))
(def path-gen (gen/fmap #(str "/" %) gen/string-alphanumeric))
(def code-gen
  (gen/frequency [[7 (gen/elements #{"200" "201" "301" "401" "403"})]
                  [3 (gen/elements #{"404" "400" "501"})]]))
(def size-gen gen/pos-int)

(defn format-date
  [l]
  (ctf/unparse (ctf/formatter "dd/MMM/yyyy:hh:mm:ss Z")
               (ctc/from-long l)))

(defn format-log-line
  [log-parts]
  (apply format "%s - - [%s] \"%s %s HTTP/1.0\" %s %s" (update log-parts 1 format-date)))

(def log-line-parts-generator
  (gen/tuple ip-gen
             date-long-gen
             method-gen
             path-gen
             code-gen
             size-gen))

(defspec log-line-parses-into-map
  100
  (prop/for-all [log-line-parts log-line-parts-generator]
                (let [log-line (format-log-line log-line-parts)
                      [ip date-long method path code size] log-line-parts]
                  (= {:ip ip :request-date (format-date date-long)
                      :method method :path path :code code}
                     (parse-log-line log-line)))))
