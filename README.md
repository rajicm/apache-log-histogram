# ASCII Apache Log histogram

A small tool that parses a set of apache access log files, and display a small histogram in ascii art of the number of requests with a given HTTP response code within each file.


## Example Histogram
```
$ lein run 404 examples/001

 Http Code: 404
  10 |
     |
     |
     | *
     | *
   5 | *     *
     | *     *
     | *   * *
     | *   * *
     | * * * * *
     |-----------
       x y z a b

  Service:
  x: www.example.com.access-20160701
  y: www1.example.com.access-20160701
  z: www2.example.com.access-20160701
  a: www3.example.com.access-20160701
  b: www4.example.com.access-20160701
```

## To run:

### Leiningen
lein run HTTP_CODE LOG_FILE_DIRECTORY

### Tests
lein test
